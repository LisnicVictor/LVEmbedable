//
//  ViewController.swift
//  LVEmbedable
//
//  Created by Lisnic_Victor on 05/05/2017.
//  Copyright (c) 2017 Lisnic_Victor. All rights reserved.
//

import UIKit
import LVEmbedable

enum Identifiers : String {
    case red, gray
}

class ViewController: UIViewController, LVEmbedable {

    @IBOutlet var embedTargetView: UIView?
    var cachedViewControllers : [String:UIViewController] = [:]

    var embededIdentifiers = [Identifiers.red.rawValue,Identifiers.gray.rawValue]

    override func viewDidLoad() {
        super.viewDidLoad()
        presentViewController(at: 0)
    }

    func presentViewController(at index:Int) {
        performSegue(withIdentifier: embededIdentifiers[index], sender: nil)
    }

    @IBAction func switchThatVC(_ sender: UISwitch) {
        presentViewController(at: sender.isOn ? 1 : 0)
    }
}

