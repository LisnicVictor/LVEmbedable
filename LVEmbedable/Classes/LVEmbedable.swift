//
//  LVEmbedable.swift
//  Pods
//
//  Created by Victor Lisnic on 5/5/17.
//
//

import UIKit

public protocol LVEmbedable : class {
    ///Array of cached UIViewController objects
    var cachedViewControllers : [String:UIViewController] {get set}

    var embedTargetView : UIView? {get}

    ///Embeds and presents provided UIViewController 
    ///No caching, no identifier is attached
    func embed(vc:UIViewController)

    ///Embeds and if it is initial presents provided UIViewController
    ///Caches UIViewController
    ///Attaches provided cacheIdentifier
    func embed(vc:UIViewController, cacheIdentifier:String)

    ///Presents UIViewController at provided cacheIdentifier
    func presentViewController(with cacheIdentifier:String)

    ///Called before viewcontroller is embeded
    ///Use this point to inject all dependencies and configuring if you are using storyboards and segues
    ///Default implementation is empty
    func willEmbed(viewControllerc:UIViewController)
}

//TODO: Implement prepare methods to add custom behaviour
public extension LVEmbedable where Self : UIViewController {

    public func embed(vc:UIViewController) {
        willEmbed(viewControllerc: vc)
        if !childViewControllers.contains(vc) {
            addChildViewController(vc)
        }
        present(vc: vc)
        vc.didMove(toParentViewController: self)
    }

    public func embed(vc:UIViewController, cacheIdentifier:String) {
        if let cachedVC = cachedViewControllers[cacheIdentifier] {
            embed(vc: cachedVC)
        } else {
            cachedViewControllers[cacheIdentifier] = vc
            embed(vc: vc)
        }
    }

    public func presentViewController(with cacheIdentifier:String) {
        guard let vc = cachedViewControllers[cacheIdentifier] else {
            return
        }
        present(vc: vc)
    }

    public func present(vc:UIViewController) {
        guard let targetView = embedTargetView else {
            return
        }

        let _ = targetView.subviews.map{$0.removeFromSuperview()}
        vc.view.frame = targetView.bounds
        targetView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        targetView.addSubview(vc.view)
    }

    public func embed(segueIdentifier:String) {
        performSegue(withIdentifier: segueIdentifier, sender: nil)
    }

    public func willEmbed(viewControllerc:UIViewController) {}
}
