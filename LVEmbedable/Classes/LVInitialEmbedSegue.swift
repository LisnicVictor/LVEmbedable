//
//  LVEmbedSegue.swift
//  Pods
//
//  Created by Victor Lisnic on 5/5/17.
//
//

import UIKit

public protocol LVEmbedableSegue {}


public class LVInitialEmbedSegue: UIStoryboardSegue , LVEmbedableSegue {

    public override func perform() {
        guard let embedable = source as? LVEmbedable else { fatalError("Fuck you") }
        embedable.embed(vc: destination, cacheIdentifier: identifier ?? "")
    }
}
