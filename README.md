# LVEmbedable

[![CI Status](http://img.shields.io/travis/Lisnic_Victor/LVEmbedable.svg?style=flat)](https://travis-ci.org/Lisnic_Victor/LVEmbedable)
[![Version](https://img.shields.io/cocoapods/v/LVEmbedable.svg?style=flat)](http://cocoapods.org/pods/LVEmbedable)
[![License](https://img.shields.io/cocoapods/l/LVEmbedable.svg?style=flat)](http://cocoapods.org/pods/LVEmbedable)
[![Platform](https://img.shields.io/cocoapods/p/LVEmbedable.svg?style=flat)](http://cocoapods.org/pods/LVEmbedable)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVEmbedable is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LVEmbedable"
```

## Author

Lisnic_Victor, gerasim.kisslin@gmail.com

## License

LVEmbedable is available under the MIT license. See the LICENSE file for more info.
